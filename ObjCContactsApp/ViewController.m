//
//  ViewController.m
//  ObjCContactsApp
//
//  Created by Douglas Sass on 2/21/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Create the table view and how it is displayed
    contactsTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    contactsTableView.translatesAutoresizingMaskIntoConstraints = NO;
    contactsTableView.delegate = self;
    contactsTableView.dataSource = self;
    contactsTableView.allowsMultipleSelectionDuringEditing = NO;
    [self.view addSubview:contactsTableView];
 
    //NSDictionary needed to create format for the table view
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(contactsTableView);
    
    NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contactsTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contactsTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    

    //Add create contact button
    UIBarButtonItem* createContactBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createBtnTouched:)];
    self.navigationItem.rightBarButtonItem = createContactBtn;

    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)loadData {
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Contact"];
    NSArray* array = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    arrayOfContacts = [array mutableCopy];
    [contactsTableView reloadData];
}

//when add Contact Button is touched it will push to the ContactDetailViewController
-(void)createBtnTouched:(id)sender {
    ContactDetailViewController* cdvc = [ContactDetailViewController new];
    [self.navigationController pushViewController:cdvc animated:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* object = arrayOfContacts[indexPath.row];
    
    cell.textLabel.text = [object valueForKey:@"lastName"];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfContacts.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ContactViewController* cvc = [ContactViewController new];
    cvc.vContact = arrayOfContacts[indexPath.row];
    [self.navigationController pushViewController:cvc animated:YES];
}

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        [arrayOfContacts removeObject:arrayOfContacts[indexPath.row]];
        [contactsTableView reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
