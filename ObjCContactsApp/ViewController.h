//
//  ViewController.h
//  ObjCContactsApp
//
//  Created by Douglas Sass on 2/21/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactDetailViewController.h"
#import "ContactViewController.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

{
    
    //Table view for the list of contacts in the app
    UITableView* contactsTableView;
    
    //Array to contain each contact and disaply them in the table view
    NSMutableArray* arrayOfContacts;
    
}


@end

