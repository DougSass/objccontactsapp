//
//  ContactDetailViewController.m
//  ObjCContactsApp
//
//  Created by Douglas Sass on 2/21/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ContactDetailViewController.h"

@interface ContactDetailViewController ()

@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    cFirstNameTxtFld = [UITextField new];
    cFirstNameTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cFirstNameTxtFld.placeholder = @" First Name";
    cFirstNameTxtFld.backgroundColor = [UIColor whiteColor];
    cFirstNameTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cFirstNameTxtFld];
    
    cLastNameTxtFld = [UITextField new];
    cLastNameTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cLastNameTxtFld.placeholder = @" Last Name";
    cLastNameTxtFld.backgroundColor = [UIColor whiteColor];
    cLastNameTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cLastNameTxtFld];
    
    cPhoneTxtFld = [UITextField new];
    cPhoneTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cPhoneTxtFld.placeholder = @" XXX-XXX-XXXX";
    cPhoneTxtFld.backgroundColor = [UIColor whiteColor];
    cPhoneTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cPhoneTxtFld];
    
    cAddressLine1TxtFld = [UITextField new];
    cAddressLine1TxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cAddressLine1TxtFld.placeholder = @" Address Line 1";
    cAddressLine1TxtFld.backgroundColor = [UIColor whiteColor];
    cAddressLine1TxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cAddressLine1TxtFld];
    
    cAddressLine2TxtFld = [UITextField new];
    cAddressLine2TxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cAddressLine2TxtFld.placeholder = @" Address Line 2";
    cAddressLine2TxtFld.backgroundColor = [UIColor whiteColor];
    cAddressLine2TxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cAddressLine2TxtFld];
    
    cCityTxtFld = [UITextField new];
    cCityTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cCityTxtFld.placeholder = @" City";
    cCityTxtFld.backgroundColor = [UIColor whiteColor];
    cCityTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cCityTxtFld];
    
    cStateTxtFld = [UITextField new];
    cStateTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cStateTxtFld.placeholder = @" State";
    cStateTxtFld.backgroundColor = [UIColor whiteColor];
    cStateTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cStateTxtFld];
    
    cZipTxtFld = [UITextField new];
    cZipTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cZipTxtFld.placeholder = @" Zip";
    cZipTxtFld.backgroundColor = [UIColor whiteColor];
    cZipTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cZipTxtFld];
    
    cEmailTxtFld = [UITextField new];
    cEmailTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    cEmailTxtFld.placeholder = @" Email Address";
    cEmailTxtFld.backgroundColor = [UIColor whiteColor];
    cEmailTxtFld.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:cEmailTxtFld];
    
    //Sets the layout of all the text fields
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(cFirstNameTxtFld,cLastNameTxtFld,cPhoneTxtFld,cAddressLine1TxtFld,cAddressLine2TxtFld,cCityTxtFld,cStateTxtFld,cZipTxtFld,cEmailTxtFld);
    
    NSDictionary *metrics = @{@"margin":[NSNumber numberWithInteger:20], @"topmargin":[NSNumber numberWithInteger:70]};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cFirstNameTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cLastNameTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cPhoneTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cAddressLine1TxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cAddressLine2TxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cCityTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cCityTxtFld]-10-[cStateTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cStateTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cZipTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[cEmailTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[cFirstNameTxtFld(33)]-5-[cLastNameTxtFld(33)]-5-[cPhoneTxtFld(33)]-5-[cAddressLine1TxtFld(33)]-5-[cAddressLine2TxtFld(33)]-5-[cCityTxtFld(33)]-5-[cStateTxtFld(33)]-5-[cZipTxtFld(33)]-5-[cEmailTxtFld(33)]" options:0 metrics:metrics views:viewsDictionary]];
    
    //Add button to save changes to the contact
    UIBarButtonItem* saveContactBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnTouched:)];
    
    //Places the Add button and Share button to the navigation bar
    self.navigationItem.rightBarButtonItems = @[saveContactBtn];
    
    //If the expense exists, display the expense
    if (self.contact) {
        cFirstNameTxtFld.text = [self.contact valueForKey:@"firstName"];
        cLastNameTxtFld.text = [self.contact valueForKey:@"lastName"];
        cPhoneTxtFld.text = [self.contact valueForKey:@"phoneNumber"];
        cAddressLine1TxtFld.text = [self.contact valueForKey:@"addressLine1"];
        cAddressLine2TxtFld.text = [self.contact valueForKey:@"addressLine2"];
        cCityTxtFld.text = [self.contact valueForKey:@"city"];
        cStateTxtFld.text = [self.contact valueForKey:@"state"];
        cZipTxtFld.text = [self.contact valueForKey:@"zip"];
        cEmailTxtFld.text = [self.contact valueForKey:@"email"];

    }
}

//Action when the save button is touched
-(void)saveBtnTouched:(id)sender {
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    //Saves changes to the expense
    if (self.contact) {
        cFirstNameTxtFld.text = [self.contact valueForKey:@"firstName"];
        cLastNameTxtFld.text = [self.contact valueForKey:@"lastName"];
        cPhoneTxtFld.text = [self.contact valueForKey:@"phoneNumber"];
        cAddressLine1TxtFld.text = [self.contact valueForKey:@"addressLine1"];
        cAddressLine2TxtFld.text = [self.contact valueForKey:@"addressLine2"];
        cCityTxtFld.text = [self.contact valueForKey:@"city"];
        cStateTxtFld.text = [self.contact valueForKey:@"state"];
        cZipTxtFld.text = [self.contact valueForKey:@"zip"];
        cEmailTxtFld.text = [self.contact valueForKey:@"email"];
    } else {
        //Creates a new expense
        NSManagedObject *newContact= [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
        [newContact setValue:cFirstNameTxtFld.text forKey:@"firstName"];
        [newContact setValue:cLastNameTxtFld.text forKey:@"lastName"];
        [newContact setValue:cPhoneTxtFld.text forKey:@"phoneNumber"];
        [newContact setValue:cAddressLine1TxtFld.text forKey:@"addressLine1"];
        [newContact setValue:cAddressLine2TxtFld.text forKey:@"addressLine2"];
        [newContact setValue:cCityTxtFld.text forKey:@"city"];
        [newContact setValue:cStateTxtFld.text forKey:@"state"];
        [newContact setValue:cZipTxtFld.text forKey:@"zip"];
        [newContact setValue:cEmailTxtFld.text forKey:@"email"];

    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
