//
//  ContactViewController.h
//  ObjCContactsApp
//
//  Created by Douglas Sass on 2/23/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>
#import "ContactDetailViewController.h"
#import <MapKit/MapKit.h>

@interface ContactViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    
    //Text field for First Name
    UILabel* vFirstNameLbl;
    
    //Text field for Last Name
    UILabel* vLastNameLbl;
    
    //Text field for phone Name
    UITextView* vPhoneTxtVw;

    //Mapview
    MKMapView* contactLocation;
    
//    //Address disaplyed on a map
//    //Address line 1
//    UITextField* vAddressLine1TxtFld;
//    
//    //Address line 2
//    UITextField* vAddressLine2TxtFld;
//    
//    //City
//    UITextField* vCityTxtFld;
//    
//    //State
//    UITextField* vStateTxtFld;
//    
//    //Zip
//    UITextField* vZipTxtFld;
    
    //Text field for Email address
    UITextView* vEmailTxtVw;
    
    
}

@property (nonatomic, strong) NSManagedObject* vContact;
@property (nonatomic, strong) NSMutableString* completeAddress;


@end
