//
//  ContactViewController.m
//  ObjCContactsApp
//
//  Created by Douglas Sass on 2/23/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ContactViewController.h"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    
    vFirstNameLbl = [UILabel new];
    vFirstNameLbl.translatesAutoresizingMaskIntoConstraints = NO;
    vFirstNameLbl.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:vFirstNameLbl];
    
    vLastNameLbl = [UILabel new];
    vLastNameLbl.translatesAutoresizingMaskIntoConstraints = NO;
    vLastNameLbl.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:vLastNameLbl];
    
    vPhoneTxtVw = [UITextView new];
    vPhoneTxtVw.translatesAutoresizingMaskIntoConstraints = NO;
    vPhoneTxtVw.dataDetectorTypes = UIDataDetectorTypeAll;
    vPhoneTxtVw.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:vPhoneTxtVw];
    
    contactLocation = [MKMapView new];
    contactLocation.translatesAutoresizingMaskIntoConstraints = NO;
    contactLocation.mapType = MKMapTypeStandard;
    [self.view addSubview:contactLocation];
    
    vEmailTxtVw = [UITextView new];
    vEmailTxtVw.translatesAutoresizingMaskIntoConstraints = NO;
    vEmailTxtVw.backgroundColor = [UIColor whiteColor];
    vEmailTxtVw.dataDetectorTypes = UIDataDetectorTypeAll;
    [self.view addSubview:vEmailTxtVw];
    
    //Sets the layout of all the text fields
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(vFirstNameLbl,vLastNameLbl,vPhoneTxtVw,contactLocation,vEmailTxtVw);
    NSDictionary *metrics = @{@"margin":[NSNumber numberWithInteger:20], @"topmargin":[NSNumber numberWithInteger:70]};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[vFirstNameLbl]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[vLastNameLbl]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[vPhoneTxtVw]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[contactLocation]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[vEmailTxtVw]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[vFirstNameLbl(40)]-5-[vLastNameLbl(40)]-5-[vPhoneTxtVw(40)]-5-[contactLocation(200)]-5-[vEmailTxtVw(40)]" options:0 metrics:metrics views:viewsDictionary]];
    

    
    //Add button to save changes to the contact
    UIBarButtonItem* updateContactBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(updateBtnTouched:)];
    
    //Share button to email the expense
    UIBarButtonItem* shareContactBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionBtnTouched:)];
    
    //Places the Add button and Share button to the navigation bar
    self.navigationItem.rightBarButtonItems = @[shareContactBtn, updateContactBtn];
    

    
    //If the contact exists, display the contact
    if (self.vContact) {
        vFirstNameLbl.text = [self.vContact valueForKey:@"firstName"];
        vLastNameLbl.text = [self.vContact valueForKey:@"lastName"];
        vPhoneTxtVw.text = [self.vContact valueForKey:@"phoneNumber"];
        vEmailTxtVw.text = [self.vContact valueForKey:@"email"];
        self.completeAddress = [NSMutableString stringWithString:[self.vContact valueForKey:@"addressLine1"]];
        [self.completeAddress appendString:@" "];
        [self.completeAddress appendString:[self.vContact valueForKey:@"addressLine2"]];
        [self.completeAddress appendString:@" "];
        [self.completeAddress appendString:[self.vContact valueForKey:@"city"]];
        [self.completeAddress appendString:@", "];
        [self.completeAddress appendString:[self.vContact valueForKey:@"state"]];
        [self.completeAddress appendString:@" "];
        [self.completeAddress appendString:[self.vContact valueForKey:@"zip"]];
        NSLog(@"%@", self.completeAddress);
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:self.completeAddress
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         if (placemarks && placemarks.count > 0) {
                             CLPlacemark *topResult = [placemarks objectAtIndex:0];
                             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                             
                             MKCoordinateRegion region = contactLocation.region;
                             region.span.longitudeDelta /= 8.0;
                             region.span.latitudeDelta /= 8.0;
                             
                             [contactLocation setRegion:region animated:YES];
                             [contactLocation addAnnotation:placemark];
                         }
                     }
         ];
        
    }
}

//when Update Contact Button is touched it will push to the ContactDetailViewController
-(void)updateBtnTouched:(id)sender {
    ContactDetailViewController* cdvc = [ContactDetailViewController new];
    cdvc.contact = self.vContact;
    [self.navigationController pushViewController:cdvc animated:YES];
}

//Sets up the email to send the expense
-(void)actionBtnTouched:(id)sender {
    
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    
    [mailCont setSubject:@"Contact"];
//    NSString* message = [NSString stringWithFormat:@"Lcoation of Expense: %@\n\nDate of Expense: %@\n\nCost of Expense: $%@\n\nNotes: %@", expLocationTxtFld.text, expDatePicker.date, expCostTxtFld.text, expNotesTxtVw.text];
//    [mailCont setMessageBody:message isHTML:NO];
    
    [self presentViewController:mailCont animated:YES completion:^{
        
    }];
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    [self dismissViewControllerAnimated:YES completion:^{
    
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
