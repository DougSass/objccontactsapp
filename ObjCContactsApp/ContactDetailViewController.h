//
//  ContactDetailViewController.h
//  ObjCContactsApp
//
//  Created by Douglas Sass on 2/21/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
//#import <Messages/Messages.h>
//#import <MessageUI/MessageUI.h>

@interface ContactDetailViewController : UIViewController
{
    
    //Text field for First Name
    UITextField* cFirstNameTxtFld;
    
    //Text field for Last Name
    UITextField* cLastNameTxtFld;
    
    //Text field for phone Name
    UITextField* cPhoneTxtFld;
    
    //Address disaplyed on a map
    //Address line 1
    UITextField* cAddressLine1TxtFld;
    
    //Address line 2
    UITextField* cAddressLine2TxtFld;
    
    //City
    UITextField* cCityTxtFld;
    
    //State
    UITextField* cStateTxtFld;
    
    //Zip
    UITextField* cZipTxtFld;
    
    //Text field for Email address
    UITextField* cEmailTxtFld;
    
    
}

@property (nonatomic, strong) NSManagedObject* contact;

@end
